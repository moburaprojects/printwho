import Vue from 'vue'
import Router from 'vue-router'
import home from './views/home'
import app from './views/app'
import booking from './views/booking'
import UpdateBooking from './views/UpdateBooking'
import login from './views/login'
import ForgotPassword from './views/ForgotPassword'
import AlreadyVerified from './views/AlreadyVerified'
import ResetPassword from './views/ResetPassword'
import signup from './views/signup'
import Service from './views/Service'
import EditService from './views/EditBooking/Service/EditNewService'
import ShipmentDetails from './views/ShipmentDetails'
import EditShipmentDetails from './views/EditBooking/Shipment/EditShipmentDetails'
import ConfirmShipment from './views/ConfirmShipment'
import EditConfirmShipment from './views/EditBooking/Confirm/EditConfirmShipment'
import UpdateShipmentDetails from './views/UpdateShipmentDetails'
import addressBook from './views/addressBook'
import Help from './views/Help'
import Reports from './views/Reports'
import Support from './views/Support'
import Middleware from './middleware.js'
import profile from './views/profile'
import settings from './views/settings'
import bookings from './views/bookings'
import TrackAndTrace from './views/trackAndTrace'
import modeOfDispatch from './views/master-data/mode-of-dispatch.vue'
import typeOfGood from './views/master-data/type-of-good.vue'
import carrier from './views/master-data/carriers.vue'
import unit from './views/master-data/unit.vue'
import division from './views/master-data/division.vue'
import hub from './views/master-data/hub.vue'
import serviceProvider from './views/master-data/service-provider.vue'
import costCenter from './views/master-data/cost-center'
import DraftList from './views/Draft/DraftList'
import EditDraftService from './views/Draft/Service/EditNewService'
import EditDraftShipment from './views/Draft/Shipment/EditDraftShipment'
import EditDraftConfirm from './views/Draft/Confirm/EditDraftConfirm'
import passwordManagement from './views/passwordManagement'
import MailRoomForm from './views/MailRoomForm'


Vue.use(Router);

const routes = [
    {
        path: '/',
        name: 'login',
        component: login
    },
    {
        path: '/verify',
        component: login,
        props: { verified: true }
    },
    {
        path: '/forgot/password',
        name: 'ForgotPassword',
        component: ForgotPassword
    },
    {
        path: '/already-verified',
        name: 'AlreadyVerified',
        component: AlreadyVerified
    },
    {
        path: '/reset/password/:token',
        name: 'ResetPassword',
        component: ResetPassword
    },
    {
        path: '/signup',
        name: 'signup',
        component: signup
    },
    {
        path: '/home',
        name: '',
        component: app,
        children: [
            {
                path: '',
                name: 'default',
                component: home,
                props: true
            },
            {
                path: 'booking',
                component: booking
            },
            {
                path: 'mailroom',
                component: MailRoomForm
            },
            {
                path: 'service',
                name: 'service',
                component: Service
            },
            {
                path: 'service/:id/edit',
                name: 'editService',
                component: EditService,
                props: true
            },
            {
                path: 'Shipment/details/:id?',
                name: 'ShipmentDetails',
                component: ShipmentDetails,
                props: false
            },
            {
                path: 'Shipment/details/:id/update',
                name: 'UpdateShipmentDetails',
                component: UpdateShipmentDetails
            },
            {
                path: 'shipment/details/:id/edit',
                name: 'editShipmentDetails',
                component: EditShipmentDetails
            },
            {
                path: 'shipment/confirm',
                name: 'confirmShipment',
                component: ConfirmShipment,
                props: true
            },
            {
                path: 'shipment/:id/confirm',
                name: 'editConfirmShipment',
                component: EditConfirmShipment
            },
            {
                path: 'addressBook',
                name: 'addressBook',
                component: addressBook
            },
            {
                path: 'help',
                name: 'Help',
                component: Help
            },
            {
                path: 'reports',
                name: 'Reports',
                component: Reports
            },
            {
                path: 'support',
                name: 'Support',
                component: Support
            },
            {
                path: 'updateBooking',
                name: 'updateBooking',
                component: UpdateBooking
            },
            {
                path: 'profile',
                name: 'profile',
                component: profile,
            },
            {
                path: 'settings',
                component: settings
            },
            {
                path: 'bookings',
                name: 'bookings',
                component: bookings
            },
            {
                path: 'trackAndTrace',
                name: 'trackAndTrace',
                component: TrackAndTrace
            },
            {
                path: 'master-data-mode-of-dispatch',
                name: 'modeOfDispatch',
                component: modeOfDispatch
            },
            {
                path: 'master-data-type-of-good',
                name: 'typeOfGood',
                component: typeOfGood
            },
            {
                path: 'master-data-carriers',
                name: 'carrier',
                component: carrier
            },
            {
                path: 'master-data-unit',
                name: 'unit',
                component: unit
            },
            {
                path: 'master-data-division',
                name: 'division',
                component: division
            },
            {
                path: 'master-data-hub',
                name: 'hub',
                component: hub
            },
            {
                path: 'master-data-service-provider',
                name: 'service-provider',
                component: serviceProvider
            },
            {
                path: 'master-data-cost-center',
                name: 'cost-center',
                component: costCenter
            },
            {
                path: 'draft-list',
                name: 'DraftList',
                component: DraftList
            },
            {
                path: 'draft/service/:id/edit',
                name: 'EditDraftService',
                component: EditDraftService,
                props: true
            },
            {
                path: 'draft/shipment/details/:id/edit',
                name: 'EditDraftShipment',
                component: EditDraftShipment,
                props: true
            },
            {
                path: 'draft/confirm/shipment/:id/edit',
                name: 'EditDraftConfirm',
                component: EditDraftConfirm,
                props: true
            },
             {
                path: 'password-management',
                name: 'password-management',
                component: passwordManagement
            }

        ]
    }
];

const router = new Router({
    routes: routes,
    mode: 'history',
    scrollBehavior (to, from, savedPosition) {
      return { x: 0, y: 0 }
    }
});

const globalMiddlewares = [
    'auth'
];

router.beforeEach((to, from, next) => {
    globalMiddlewares.forEach((middleware) => Middleware[middleware](to,from,next));
})

export default router;
