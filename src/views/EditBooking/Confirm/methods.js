import router from '../../../router';

export default {
    fetchShippingDetails() {

    },
    editDetails(pos) {
        router.push({ name: 'service', params: { pos: pos } });
    },
    editShipmentDetails(pos) {
        router.push({ name: 'ShipmentDetails', params: { pos: pos } });
    },
    fillSenderDetails() {
        // let senderId = JSON.parse(localStorage.senderDetails)[0];
        // console.log('sender:', senderId);
        // let currentAddress = this.addresses.find(address => address.id == senderId);
        let bookingData = JSON.parse(localStorage.courierDetails);
        let currentBookingDetails = bookingData.find(booking => booking.id == this.$route.params.id );
        console.log('fillsender detils', currentBookingDetails);
        this.senderName = !this.isUser ? this.requesterName : this.user.name ;
        this.branchId = JSON.parse(localStorage.courierBookingData).branch_id;
        this.branchList = JSON.parse(localStorage.companyDetails)[0].branches;
        this.currentBranch = this.branchList.find(branch => branch.id == this.branchId);
        this.senderBranch = !this.isUser ? this.user.branch.name : '';
        this.senderAddress1 = !this.isUser ? currentBookingDetails.requester_address_1 : '' ;
        this.senderAddress2 = !this.isUser ? currentBookingDetails.requester_address_2 : '' ;
        this.senderCity = !this.isUser ? currentBookingDetails.requester_city : '' ;
        this.senderPostalCode = !this.isUser && currentBookingDetails.requester_postal_code ? currentBookingDetails.requester_postal_code : '' ;
        this.senderCountry = !this.isUser ? currentBookingDetails.requester_country : '' ;
        this.senderPhone = !this.isUser ? currentBookingDetails.requester_phone : '' ;
        this.senderEmail = !this.isUser ? currentBookingDetails.requester_email : '' ;

    },
    fillShipmentDetails() {
        let bookingData = JSON.parse(localStorage.courierBookingData);
        this.shipmentDate = bookingData.pickup_date;
        this.packgingType = bookingData.type_of_good;
        this.numberOfPieces = bookingData.item_qty;
        this.goodsDescription = bookingData.goods_description;
        this.remarks = bookingData.remarks;
        this.totalWeight = bookingData.item_qty * bookingData.item_weight;

    },
    fillPickupDetails() {
        // let currentPickupAddress = JSON.parse(localStorage.pickupDetails);
        // console.log('pickup:', pickupId);
        // let currentAddress = this.addresses.find(address => address.id == pickupId);
        let bookingData = JSON.parse(localStorage.courierBookingData);
        this.pickupName = bookingData.pickup_name;
        this.pickupBranch = bookingData.pickup_branch;
        this.pickupAddress1 = bookingData.pickup_address_1;
        this.pickupAddress2 = bookingData.pickup_address_2;
        this.pickupCity = bookingData.pickup_city;
        this.pickupPostalCode = bookingData.pickup_postal_code;
        this.pickupCountry = bookingData.pickup_country;
        this.pickupPhone = bookingData.pickup_phone;
        this.pickupEmail = bookingData.pickup_email;
    },
    fillDeliveryDetails() {
        // let deliveryIds = JSON.parse(localStorage.deliveryDetails);
        let bookingData = JSON.parse(localStorage.courierBookingData);
        // console.log('delivery:', deliveryIds);
        // let currentAddress = this.addresses.find(address => address.id == deliveryIds[0]);
        this.deliveryName = bookingData.delivery_address[0].delivery_name;
        this.deliveryBranch = bookingData.delivery_address[0].delivery_branch;
        this.deliveryAddress1 = bookingData.delivery_address[0].delivery_address_1;
        this.deliveryAddress2 = bookingData.delivery_address[0].delivery_address_2;
        this.deliveryCity = bookingData.delivery_address[0].delivery_city;
        this.deliveryPostalCode = bookingData.delivery_address[0].delivery_postal_code;
        this.deliveryCountry = bookingData.delivery_address[0].delivery_country;
        this.deliveryPhone = bookingData.delivery_address[0].delivery_phone;
        this.deliveryEmail = bookingData.delivery_address[0].delivery_email;

        this.selectedDeliveryCount = bookingData.delivery_address.length;

    },
    notifyEmail(value) {
        if(value == 'shiper') {
            this.notifyShiperEmail = !this.notifyShiperEmail;
        }
        else if(value == 'receiver') {
            this.notifyReceiverEmail = !this.notifyReceiverEmail;
        }
        else if(value == 'proActive'){
            this.proActiveNotification = !this.proActiveNotification;
        }
    },
    termsAccepted() {
        this.termsAgree = !this.termsAgree;
    },
    submitBookingDetails() {
        this.courierBookingData.notify_shiper_email = this.notifyShiperEmail ? this.shiperEmail: '';
        this.courierBookingData.notify_receiver_email = this.notifyReceiverEmail ? this.receiverEmail: '';
        this.courierBookingData.pro_active_notification = this.proActiveNotification ? 1 : 0; 
        this.courierBookingData.requester_address_1 = this.senderAddress1;
        this.courierBookingData.requester_address_2 = this.senderAddress2;
        this.courierBookingData.requester_city = this.senderCity;
        this.courierBookingData.requester_postal_code = this.senderPostalCode;
        this.courierBookingData.requester_country = this.senderCountry;
        this.courierBookingData.requester_phone = this.senderPhone;
        this.courierBookingData.requester_email = this.senderEmail;
        console.log('hello', this.courierBookingData);

        let data = JSON.parse(localStorage.courierBookingData);
        let updatedData = Object.assign(data, this.courierBookingData);
        updatedData = Object.assign(updatedData, updatedData.delivery_address[0]);
        delete updatedData['delivery_address'];
        localStorage.courierBookingData = JSON.stringify(updatedData);
        console.log('c:', this.courierBookingData);
        console.log('data:', updatedData);

        this.checkEmptyValue(updatedData);
    },
    checkEmptyValue(updatedData) {
        updatedData.booking_type = 'domestic';
        let flag = true;
        Object.keys(this.courierBookingData).forEach((key) => {
            if(!this.courierBookingData[key] && key != 'notify_shiper_email' && key != 'notify_receiver_email' && key != 'pro_active_notification' && key != 'requester_address_1' && key != 'requester_address_2' && key != 'requester_city' && key != 'requester_postal_code' && key != 'requester_country' && key != 'requester_phone' && key != 'requester_email') {
                console.log(key);
                flag = false;
            }
        });
        if(!flag || !this.termsAgree) {
            let msg;
            if(!this.termsAgree)
                msg = "Please Agree Terms and conditions";
            else 
                msg = "Please fill All Fields";

            this.$toasted.show(msg, { 
                 theme: "primary", 
                 position: "bottom-right", 
                 type: 'error',
                 duration: 2000,
                 iconPack : 'material',
                 icon : {
                    name : 'close'
                 }
            });
        }
        else {

            console.log('updatedData => ',updatedData);
            this.preloader = true;
             this.axios.put(`courier/domestic/${this.$route.params.id}/edit`, updatedData).
                then(response => {
                    console.log(response);
                    if(response.data.status == 'ok') {
                        this.preloader = false;
                        this.$toasted.show("Your Booking Successfully edited", { 
                             theme: "primary", 
                             position: "bottom-right", 
                             type: 'success',
                             duration: 2000,
                             iconPack : 'material',
                             icon : {
                                name : 'check'
                             }
                        });
                        setTimeout(() => {
                            router.push({name: 'updateBooking'});
                            location.reload();
                        }, 1000);
                    }                  
                }).
                catch((error) => {
                    console.log(error);
                    this.preloader = false;
                    this.$toasted.show("Some error occured..Please try again..", { 
                         theme: "primary", 
                         position: "bottom-right", 
                         type: 'error',
                         duration: 2000,
                         iconPack : 'material',
                         icon : {
                            name : 'error'
                         }
                    });
                })
        }
    }
}
