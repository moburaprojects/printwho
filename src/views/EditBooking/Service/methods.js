import router from '../../../router';

export default {
    fetchCourier(id) {
        console.log('called');
        this.axios.get(`courier/domestic/${id}`).
        then(response => {
            localStorage.currentCourier = JSON.stringify(response.data.courier);
            this.fillPageDetails(response.data.courier);
            localStorage.delivery_address = JSON.stringify(response.data.courier.delivery_address);
        }).
        catch(error => {
            console.log(error);
       })
    },

    /**
     * Fill the courier details
     * 
     * @param {[uuid]} id [uuid of the courier]
     */
    setCourier(id) {
        let couriers = JSON.parse(localStorage.courierDetails);
        let currentCourier = couriers.find(courier => courier.id == id);
        this.fillPageDetails(currentCourier);
    },

    /**
     * Set the pickup name.
     * 
     * @return void
     */
    setPickupName(name) {
        console.log('called set pickupname',name);
        this.pickupName = name;
    },

    /**
     * Set requester name.
     * 
     * @param {string} name 
     */
    setRequesterName(name) {
        console.log('called set requestername',name);
        this.requesterName = name;
    },

    /**
     * Select the rate card.
     * 
     * @param  {object} provider 
     * @return {void}          
     */
    selectRateCard(provider) {
        if(this.$refs[provider.id][0].checked) {
            this.delivery_provider = provider.name;
            console.log('provider aane:',provider);
            this.carrierCode = provider.carrier_code;
            this.deliveryProviderId = provider.id;
            this.selectedDeliveryServices = provider;
            this.service = this.selectedDeliveryServices.name + '@' + this.selectedDeliveryServices.id;
        }
        // else {
        //     let index = this.selectedDeliveryServices.findIndex(service => service.id == provider.id);
        //     this.selectedDeliveryServices.splice(index);
        // }
    },

    /**
     * Fill the pickup details.
     * 
     * @param  {[type]} address 
     * @return {void}  
     */
    pickupDetailsFill(address) {
        console.log(address);
        if(address != undefined) {
            this.pickupName = address.name;
            this.pickupAddress1 = address.address_1;
            this.pickupAddress2 = address.address_2;
            this.pickupPostalCode = address.postal_code;
            this.pickupCity = address.city;
            this.pickupSuburb = address.suburb;
            this.pickupState = address.state;
            this.pickupCountry = address.country;
            this.pickupPhone = address.phone;
            this.pickupMobile = address.mobile ? addr.mobile : '';
            this.pickupEmail = address.email;
        }
    },

    /**
     * Fill Delivery Details.
     * 
     * @param  {object} address 
     * @return {void}         
     */
    deliveryDetailsFill(address) {
        console.log(address);
        if(address != undefined) {
            this.deliveryName = address.name;
            this.deliveryCompanyName = address.company_name;
            this.deliveryBranchName = address.branch_name;
            this.deliveryAddress1 = address.address_1;
            this.deliveryAddress2 = address.address_2;
            this.deliveryPostalCode = address.postal_code;
            this.deliveryCity = address.city;
            this.deliverySuburb = address.suburb;
            this.deliveryState = address.state;
            this.deliveryCountry = address.country;
            this.deliveryPhone = address.phone;
            this.deliveryMobile = address.mobile ? addr.mobile : '';
            this.deliveryEmail = address.email;
        }
    },

    /**
     * SetPriority Flag
     * 
     */
    setPriority() {
        this.hasPriority = !this.hasPriority;
    },
    pickupNameShow(item) {
        this.pickupList = true;
        this.$refs[item].classList.remove('warning');

    },
    deliveryNameShow(item) {
        this.deliveryList = true;
        this.$refs[item].classList.remove('warning');
    },

    /**
     * Close Ratecard
     * 
     * @param  {[type]} data [description]
     * @return {[type]}      [description]
     */
    closeRateCard(data) {
        this.rateCard = false;
    },
    rateCardMessage() {
        this.rateCard = true;
        // else {
        //     this.$toasted.show("No Rate card to Display", { 
        //          theme: "primary", 
        //          position: "bottom-right", 
        //          type: 'success',
        //          duration: 2000,
        //          iconPack : 'material',
        //          icon : {
        //             name : 'error'
        //          }
        //     });
        // }
    },
    populatePickupDetails() {
        console.log('populating pickup',this.userDetails.name);
        this.pickupName = this.userDetails.name;
        this.pickupAddress1 = this.userDetails.address_1;
        this.pickupAddress2 = this.userDetails.address_2;
        this.pickupPostalCode = this.userDetails.postal_code;
        this.pickupCity = this.userDetails.city;
        this.pickupSuburb = this.userDetails.suburb;
        this.pickupState = this.userDetails.state;
        this.pickupCountry = this.userDetails.country;
        this.pickupPhone = this.userDetails.phone;
        this.pickupMobile = this.userDetails.mobile ? this.userDetails.mobile : '';
        this.pickupEmail = this.userDetails.email;
        this.pickupBranchName = this.userDetails.branch.branch_id;
        this.pickupCompanyName = 'WHO';

    },
    
    /**
     * Fill Page Details.
     * 
     * @param  {object} courier [description]
     * @return {[type]}         [description]
     */
    fillPageDetails(courier) {
        // let courierDetails = JSON.parse(localStorage.courierDetails);
        let currentCourier = courier;
        console.log('current courier is: ', currentCourier);
        this.entryType = currentCourier.entry_type;
        this.requesterName = currentCourier.requester_name;
        this.requesterBranch = currentCourier.branch_id;
        this.requestDate = currentCourier.request_date;
        this.modeOfDispatch = currentCourier.mode_of_dispatch;
        console.log('mode of dispatch is', this.modeOfDispatch);
        this.addProvider(currentCourier.mode_of_dispatch);
        // console.log(JSON.stringify(this.$refs));
        console.log('delivery provider', currentCourier.provider_id);
        let a = document.getElementById(currentCourier.provider_id);
        console.log(this.providerDetails)
        console.log('a',a);

        this.itemType = currentCourier.type_of_good;
        // this.service = currentCourier.delivery_service + '@' + currentCourier.service_id;
        this.referenceNo = currentCourier.reference_no;
        this.hasPriority = currentCourier.has_priority;
        if(this.hasPriority) {
            this.$refs.priorityFlag.checked = true;
        }
        this.PTAEO = currentCourier.p + '/' + currentCourier.t + '/' +
                     currentCourier.a + '/' + currentCourier.e + '/' +
                     currentCourier.o;

        this.pickupName = currentCourier.pickup_name; 
        this.pickupCompanyName = currentCourier.pickup_company; 
        this.pickupBranchName = currentCourier.pickup_branch;
        this.pickupAddress1 = currentCourier.pickup_address_1; 
        this.pickupAddress2 = currentCourier.pickup_address_2; 
        this.pickupPostalCode = currentCourier.pickup_postal_code;
        this.pickupCity = currentCourier.pickup_city;
        this.pickupSuburb = currentCourier.pickup_suburb;
        this.pickupState = currentCourier.pickup_state;
        this.pickupCountry = currentCourier.pickup_country;
        this.pickupMobile = currentCourier.pickup_mobile;
        this.pickupPhone = currentCourier.pickup_phone;
        this.pickupEmail = currentCourier.pickup_email; 

        this.deliveryName = currentCourier.delivery_name;
        this.deliveryCompanyName = currentCourier.delivery_company; 
        this.deliveryBranchName = currentCourier.delivery_branch; 
        this.deliveryAddress1 = currentCourier.delivery_address_1; 
        this.deliveryAddress2 = currentCourier.delivery_address_2; 
        this.deliveryPostalCode = currentCourier.delivery_postal_code;
        this.deliveryCity = currentCourier.delivery_city;
        this.deliverySuburb = currentCourier.delivery_suburb;
        this.deliveryState = currentCourier.delivery_state;
        this.deliveryCountry = currentCourier.delivery_country;
        this.deliveryMobile = currentCourier.delivery_mobile;
        this.deliveryPhone = currentCourier.delivery_phone;
        this.deliveryEmail = currentCourier.delivery_email;

    },
    fillPickupDetails(selectedAddresses) {
        console.log(selectedAddresses);
        this.showAddressBook = false;
        if(this.pointOfCall == 'delivery-data')
            this.courierBookingData.delivery_address = [];
        let addresses = JSON.parse(localStorage.addresses);
        selectedAddresses.map((addr) => {
            addresses.map((address) => {
                if(address.id == addr ) {
                    if(this.pointOfCall == 'pickup-data') {
                        this.pickupPreviousSelection = selectedAddresses;
                        this.pickupName = address.name;
                        this.pickupCompanyName = address.company_name;
                        this.pickupBranchName = address.branch_name;
                        this.pickupAddress1 = address.address_1;
                        this.pickupAddress2 = address.address_2;
                        this.pickupPostalCode = address.postal_code;
                        this.pickupCity = address.city;
                        this.pickupSuburb = address.suburb ? address.suburb : '';
                        this.pickupState = address.state ? address.state : '';
                        this.pickupCountry = address.country ? address.country : '';
                        this.pickupPhone = address.phone;
                        this.pickupEmail = address.email;
                    }
                    else if(this.pointOfCall == 'delivery-data') {
                        this.deliveryList = false;
                        this.deliveryPreviousSelection = selectedAddresses;
                        this.pickupAddressCount = selectedAddresses ? selectedAddresses.length: 0;
                        this.deliveryName = address.name;
                        this.deliveryCompanyName = address.company_name;
                        this.deliveryBranchName = address.branch_name;
                        this.deliveryAddress1 = address.address_1;
                        this.deliveryAddress2 = address.address_2;
                        this.deliveryPostalCode = address.postal_code;
                        this.deliveryCity = address.city;
                        this.deliverySuburb = address.suburb ? address.suburb : '';
                        this.deliveryState = address.state ? address.state : '';
                        this.deliveryCountry = address.country ? address.country : '';
                        this.deliveryPhone = address.phone;
                        this.deliveryEmail = address.email;
                        this.storeDeliveryData();
                    }
                    else if(this.pointOfCall == 'service-data') {
                        localStorage.sender = JSON.stringify(address);
                        this.requesterName = address.name;
                        this.requesterBranch = address.branch_name;
                    }
                }
            });
        });

    },

    /**
     * Select the entry type.
     * 
     * @param  {string} value [description]
     * @return {void}       [description]
     */
    entryMethod(value) {
        if(value == 'single') {
            this.entryType = 'single';
        }
        else {
            this.entryType = 'batch';
        }
    },

    /**
     * Show the address modal.
     * 
     * @param {string} point [point of call]
     */
    ShowAddressModal(point) {
        this.showAddressBook = true;
        this.pointOfCall = point;
        if(this.pointOfCall == 'pickup-data') {
            this.previousSelection = this.pickupPreviousSelection;
        }
        else if(this.pointOfCall == 'delivery-data') {
            this.previousSelection = this.deliveryPreviousSelection;
        }
    },
    
    storeDeliveryData(){
        let deliverypoint = {
            delivery_name : this.deliveryName,
            delivery_company: this.deliveryCompanyName,
            delivery_branch: this.deliveryBranchName,
            delivery_address_1: this.deliveryAddress1,
            delivery_address_2: this.deliveryAddress2,
            delivery_postal_code: this.deliveryPostalCode,
            delivery_city: this.deliveryCity,
            delivery_suburb: this.deliverySuburb,
            delivery_state: this.deliveryState,
            delivery_country: this.deliveryCountry,
            delivery_mobile: this.deliveryMobile,
            delivery_email: this.deliveryEmail
        }
        this.courierBookingData.delivery_address.push(deliverypoint);
    },
    storeData() {
        // this.courierBookingData.delivery_address = [];
        this.service = document.getElementById('serviceProvided').value;
        localStorage.currentEditGood = this.itemType;
        let deliverypoint = {
            delivery_name : this.deliveryName,
            delivery_company: this.deliveryCompanyName,
            delivery_branch: this.deliveryBranchName,
            delivery_address_1: this.deliveryAddress1,
            delivery_address_2: this.deliveryAddress2,
            delivery_postal_code: this.deliveryPostalCode,
            delivery_city: this.deliveryCity,
            delivery_suburb: this.deliverySuburb,
            delivery_state: this.deliveryState,
            delivery_country: this.deliveryCountry,
            delivery_mobile: this.deliveryMobile,
            delivery_email: this.deliveryEmail
        }
        this.courierBookingData.delivery_address.splice(-1,1);
        this.courierBookingData.delivery_address.push(deliverypoint);


        this.courierBookingData.entry_type = this.entryType;
        this.courierBookingData.requester_name = this.requesterName;
        // let temp_branch = this.requesterBranch;
        let temp_company = JSON.parse(localStorage.companyDetails)[0].branches;
        let t = temp_company.find(item => item.id == this.requesterBranch);

        this.courierBookingData.branch_id = t.id;
        this.courierBookingData.delivery_provider = this.delivery_provider;
        console.log('provider', this.delivery_provider);
        this.courierBookingData.provider_id = this.deliveryProviderId;
        // this.courierBookingData.branch_id = JSON.parse(localStorage.user).branch_id;
        this.courierBookingData.request_date = this.requestDate;
        this.courierBookingData.type_of_good = this.itemType;
        this.courierBookingData.mode_of_dispatch = this.modeOfDispatch;
        this.courierBookingData.delivery_service = this.service.split('@')[0];
        this.courierBookingData.service_id = this.service.split('@')[1];
        this.courierBookingData.pickup_name = this.pickupName;
        this.courierBookingData.pickup_company = this.pickupCompanyName;
        this.courierBookingData.pickup_branch = this.pickupBranchName;
        this.courierBookingData.pickup_address_1 = this.pickupAddress1;
        this.courierBookingData.pickup_address_2 = this.pickupAddress2;
        this.courierBookingData.pickup_postal_code = this.pickupPostalCode;
        this.courierBookingData.pickup_city = this.pickupCity;
        this.courierBookingData.pickup_suburb = this.pickupSuburb;
        this.courierBookingData.pickup_state = this.pickupState;
        this.courierBookingData.pickup_country = this.pickupCountry;
        this.courierBookingData.pickup_mobile= this.pickupMobile;
        this.courierBookingData.pickup_phone= this.pickupPhone;
        this.courierBookingData.pickup_email = this.pickupEmail;
        this.courierBookingData.reference_no = this.referenceNo;
        let PTAEO = this.PTAEO.split('/');
        this.courierBookingData.p = PTAEO[0];
        this.courierBookingData.t = PTAEO[1];
        this.courierBookingData.a = PTAEO[2];
        this.courierBookingData.e = PTAEO[3];
        this.courierBookingData.o = PTAEO[4];
        this.courierBookingData.has_priority = this.hasPriority? 1 : 0;

        console.log(this.courierBookingData);

        this.checkEmptyValue(deliverypoint);


        localStorage.courierBookingData = JSON.stringify(this.courierBookingData);


        localStorage.pickupDetails = JSON.stringify({
            pickupName : this.pickupName ,
            pickupCompanyName : this.pickupCompanyName ,
            pickupBranchName : this.pickupBranchName ,
            pickupAddress1 : this.pickupAddress1 ,
            pickupAddress2 : this.pickupAddress2 ,
            pickupPostalCode : this.pickupPostalCode ,
            pickupCity : this.pickupCity ,
            pickupSuburb : this.pickupSuburb  ? this.pickupSuburb : '',
            pickupState : this.pickupState  ? this.pickupState : '',
            pickupCountry : this.pickupCountry  ? this.pickupCountry : '',
            pickupPhone : this.pickupPhone ,
            pickupEmail : this.pickupEmail 
        })
    },

    /**
     * Add the delivery provider and its services.
     * 
     * @param {[type]} newValue [description]
     */
    addProvider(newValue) {
        this.providerDetails = [];
        let currentDispatch = this.dispatchDetails.find(item => item.name == newValue);
        console.log('CurrentDispatch:', currentDispatch);
        this.deliveryServices = [];
        let courierProvider;
        currentDispatch.providers.forEach(provider => {
            this.providerDetails.push(provider);
            if(currentDispatch.name == 'Courier' && provider.carrier_code == 'WHOC') {
                courierProvider = provider;
                if(provider.services.length > 0) {
                    this.deliveryServices.push(provider.services[0]);
                }
            }
        });
        if(currentDispatch.name == 'Courier' && this.isUser) {
            setTimeout(() => {
                this.$refs[courierProvider.id][0].checked = true;
                this.$refs[courierProvider.id][0].click();
                this.service = courierProvider.services[0].name + '@' + courierProvider.services[0].id;
            })
        }
        console.log('Provider details:', this.providerDetails);
        // this.service = this.deliveryServices[0].id + '@' + this.deliveryServices[0].name;
    },

    warningBox(key) {
        // this.$refs[key].class = 'warning';
        this.$refs[key].classList.add('warning');
    },
    removeWarning(item) {
        this.$refs[item].classList.remove('warning');
    },

    /**
     * Validation.
     * 
     * @param  {[type]} deliverypoint [description]
     * @return {[type]}               [description]
     */
    checkEmptyValue(deliverypoint) {
        let flag = true;
        let errorMessage; 
        Object.keys(this.courierBookingData).forEach((key) => {
            if(!this.courierBookingData[key] && key != 'pickup_suburb' && key != 'pickup_mobile' && key != 'pickup_state' && key != 'pickup_postal_code' && key != 'pickup_phone' && key != 'pickup_email' && key != "pickup_address_2" && key != 'has_priority' && key != 'reference_no' && key != 'service_id') {
                console.log('key:', typeof(key));
                if( key == 'p' || key == "t" || key == "a" || key == "e" || key == "o") {
                    key = "ptaeo";
                }
                this.warningBox(key);
                flag = false;
                // errorMessage = key + ' is required';
                // flag = false;
                // this.$toasted.show(errorMessage, { 
                //      theme: "primary", 
                //      position: "bottom-right", 
                //      type: 'error',
                //      duration: 2000,
                //      iconPack : 'material',
                //      icon : {
                //         name : 'close'
                //      }
                // });
            }
        });
        this.checkDeliveryEmptyValue(deliverypoint, flag);
    },
    /**
     * Check Delivery Values.
     * 
     * @param  {[type]} deliverypoint [description]
     * @param  {[type]} flag          [description]
     * @return {[type]}               [description]
     */
    checkDeliveryEmptyValue(deliverypoint, flag) {
        console.log('delivery point :', deliverypoint);
        let deliveryErrorMessage;
        let deliveyAddressLength = this.courierBookingData.delivery_address.length;
        Object.keys(deliverypoint).forEach((key) => {
            if(!deliverypoint[key] && key != 'delivery_suburb' && key != 'delivery_mobile' && key != 'delivery_postal_code' && key != 'delivery_state' && key != 'delivery_email' && key != "delivery_address_2" && key != 'delivery_phone') {
                console.log(key);
                deliveryErrorMessage = key + ' is required';
                this.warningBox(key);
                // this.$toasted.show(deliveryErrorMessage, { 
                //      theme: "primary", 
                //      position: "bottom-right", 
                //      type: 'error',
                //      duration: 2000,
                //      iconPack : 'material',
                //      icon : {
                //         name : 'close'
                //      }
                // });
                flag = false;
            }
        });

        if(flag) {
            if(this.changeGood) {
                console.log('dcdd');
                localStorage.currentGoods = JSON.stringify(this.currentGoods);
            }
            //localStorage.currentGoods = JSON.stringify(this.currentGoods);
            console.log('currentGoods:', this.currentGoods);
            router.push({ name: 'editShipmentDetails',params: { id: this.$route.params.id }});
        } else {
            this.$toasted.show('Please fill all details', { 
                 theme: "primary", 
                 position: "bottom-right", 
                 type: 'error',
                 duration: 2000,
                 iconPack : 'material',
                 icon : {
                    name : 'close'
                 }
            });
        }
    }

}
