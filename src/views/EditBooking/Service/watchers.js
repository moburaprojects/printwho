export default {
    requesterName: function(newValue, oldValue) {
        this.pickupName = newValue;
        console.log('requesterName => ',newValue);
    },
    pickupName: function(newValue, oldValue) {
        console.log('pickupname => ',newValue);
        if(newValue !== '') {
            let searchAddress = [];
            let searchTerm = newValue.toLowerCase();
            this.addressesArray.forEach(address => {

                if(address.name.toLowerCase().includes(searchTerm)) {
                    searchAddress.push(address);
                }
            });
            this.pickupAddressList = searchAddress;
        } 
        else {
            this.pickupAddressList = this.addressesArray;
        }
        
    },
    deliveryName: function(newValue, oldValue) {
        console.log('deliveryname => ',newValue);
        if(newValue !== '') {
            let searchAddress = [];
            let searchTerm = newValue.toLowerCase();
            this.addressesArray.forEach(address => {

                if(address.name.toLowerCase().includes(searchTerm)) {
                    searchAddress.push(address);
                }
            });
            this.deliveryAddressList = searchAddress;
        } 
        else {
            this.deliveryAddressList = this.addressesArray;
        }
    },
    selectedDeliveryServices(newValue, oldValue) {
        console.log('Hello:',newValue);
        this.deliveryServices = [];
        this.currentProvider = newValue;
        this.currentProvider.services.forEach(item => {
                this.deliveryServices.push(item);
        });
        console.log('List',this.deliveryServices);
    },
    toLower(str){
        return (str == null) ? '' : str.toLowerCase();
    },
    itemType: function(newValue, oldValue) {
        this.currentGoods = this.goodsList.find(item => item.type == newValue);
        if(oldValue != '') {
            this.changeGood = true; 
        }
        if(localStorage.currentGoods && newValue != JSON.parse(localStorage.currentGoods).type) {
            console.log('goods changed');    
        }
        else {
            this.currentGoods = this.goodsList.find(item => item.type == newValue); 
            if(!this.currentGoods) {
                this.currentGoods = this.goodsList[0];
            }
        }
        this.goodsDispatchDetails = this.currentGoods.modes;
    },
    modeOfDispatch: function(newValue, oldValue) {
        this.addProvider(newValue);  
    },
    service: function(newValue, oldValue) {
        console.log('provider',this.currentProvider);
        // let serviceId = newValue.split('@')[0];
        // let currentService = this.deliveryServices.find(item => item.id == serviceId);
        // let providerId = (currentService.provider_id);
        // this.currentProvider = this.providerDetails.find(data => data.id == currentService.provider_id);     
        this.providerLogo = this.currentProvider.logo; 
        this.providerDescription = this.currentProvider.description;      
    }
}
