import router from '../../../router';

export default {
    /**
     * Fill the courier details
     * 
     * @param {[uuid]} id [uuid of the courier]
     */
    setCourier(id) {
        let couriers = JSON.parse(localStorage.courierDetails);
        let goods = JSON.parse(localStorage.goods);
        let currentCourier = couriers.find(courier => courier.id == id);
        if(currentCourier.type_of_good == localStorage.currentEditGood) {
            this.currentGoods = goods.find(item => item.type == currentCourier.type_of_good);
            this.fillData(currentCourier);
            this.fillOldDetails(currentCourier);
        } else {
            this.currentGoods = goods.find(item => item.type == localStorage.currentEditGood);
            this.fillDetails();
        }

        if(this.currentGoods.volume == 'N') {
            this.isDisabled = true;
        }
        else if(this.currentGoods.volume == 'Y') {
            this.isDisabled = false;
        }
        
    },
    fillOldDetails(currentCourier) {
        this.currentWeight = currentCourier.item_weight ? currentCourier.item_weight : '0';
        this.currentWidth = currentCourier.item_width ? currentCourier.item_width : '0';
        this.currentHeight = currentCourier.item_height ? currentCourier.item_height : '0';
        this.currentLength = currentCourier.item_length ? currentCourier.item_length : '0';
        this.currentm3 = currentCourier.item_m3 ? currentCourier.item_m3 : '0';
        this.currentCubicKg = currentCourier.item_cubic_kg ? currentCourier.item_cubic_kg : '0';
        this.currentChargeUnit = currentCourier.item_charge_unit ? currentCourier.item_charge_unit : '0'

    },
    fillDetails() {
        this.currentWeight = this.currentGoods.weight ? this.currentGoods.weight : '0';
        this.currentWidth = this.currentGoods.width ? this.currentGoods.width : '0';
        this.currentHeight = this.currentGoods.height ? this.currentGoods.height : '0';
        this.currentLength = this.currentGoods.length ? this.currentGoods.length : '0';
        this.currentm3 = this.currentGoods.m3 ? this.currentGoods.m3 : '0';
        this.currentCubicKg = this.currentGoods.cubic_kg ? this.currentGoods.cubic_kg : '0';
        this.currentChargeUnit = this.currentGoods.charge_unit ? this.currentGoods.charge_unit : '0'

    },
    fillData (currentCourier) {
        //let currentCourier = JSON.parse(localStorage.currentCourier);
        this.itemName = currentCourier.item_reference;
        this.itemCount = currentCourier.item_qty;
        this.goodsDescription = currentCourier.goods_description;
        this.remarks = currentCourier.remarks;
        this.packageLocation = currentCourier.package_location;
        this.pickupInstructions = currentCourier.pickup_instructions;
        this.pickupDate = currentCourier.pickup_date;
        this.pickupTime = currentCourier.pickup_time;
        this.officeCloseTime = currentCourier.office_close_time;         
    },
     storeData() {
        console.log('currentGoods:', this.currentGoods);
        this.courierBookingData.item_reference = this.itemName;
        this.courierBookingData.item_weight = this.currentWeight? this.currentWeight : '0' ;
        this.courierBookingData.item_qty = this.itemCount;
        this.courierBookingData.item_length = this.currentLength;
        this.courierBookingData.item_width = this.currentWidth;
        this.courierBookingData.item_height = this.currentHeight;
        this.courierBookingData.item_m3 = this.currentm3;
        this.courierBookingData.item_cubic_kg = this.currentCubicKg;
        this.courierBookingData.item_charge_unit = this.currentChargeUnit;
        this.courierBookingData.item_is_dg = this.currentGoods.is_dg ? '1' : '0';
        this.courierBookingData.item_is_food = this.currentGoods.is_food ? '1' : '0';
        this.courierBookingData.goods_description = this.goodsDescription;
        this.courierBookingData.remarks = this.remarks;
        this.courierBookingData.package_location = this.packageLocation;
        this.courierBookingData.pickup_instructions = this.pickupInstructions;
        this.courierBookingData.pickup_date = this.pickupDate;
        this.courierBookingData.pickup_time = this.pickupTime;
        this.courierBookingData.office_close_time = this.officeCloseTime;

        let itemDetails = {
            weight: this.currentWeight,
            width: this.currentWidth,
            height: this.currentHeight,
            length: this.currentLength,
            m3: this.currentm3,
            cubic_kg: this.currentCubicKg,
            charge_unit: this.currentChargeUnit
        }
        console.log('itemDetails:', itemDetails);
        let temp = JSON.parse(localStorage.currentGoods);

        let updatedItemDetails = Object.assign(temp, itemDetails);
        console.log('updated item details:', updatedItemDetails);
        localStorage.currentGoods = JSON.stringify(updatedItemDetails);

        let data = JSON.parse(localStorage.courierBookingData);
        let updatedData = Object.assign(data, this.courierBookingData);
        localStorage.courierBookingData = JSON.stringify(updatedData);
        console.log('c:', this.courierBookingData);
        console.log('data:', updatedData);

        this.checkEmptyValue();
    },
     warningBox(key) {
        this.$refs[key].classList.add('warning');
    },
    removeWarning(item) {
        this.$refs[item].classList.remove('warning');
    },
    checkEmptyValue() {
        let flag = true;
       Object.keys(this.courierBookingData).forEach((key) => {
                    if(!this.courierBookingData[key] && key != 'item_reference' && key != 'item_width' && key != 'item_height' && key != 'item_m3' && key != 'item_cubic_kg' && key != 'item_charge_unit' && key != 'item_length' && key != 'goods_description' && key != 'remarks') {
                        console.log(key);
                        this.warningBox(key);
                        // this.$toasted.show(key + " is required", { 
                        //      theme: "primary", 
                        //      position: "bottom-right", 
                        //      type: 'error',
                        //      duration: 2000,
                        //      iconPack : 'material',
                        //      icon : {
                        //         name : 'close'
                        //      }
                        // });
                        flag = false;
                    }
                });
        if(flag) {
            router.push({
                name: 'editConfirmShipment',
                params: {
                    id: this.$route.params.id
                }
            });
        }
        else {
            this.$toasted.show("All fields are required", { 
                 theme: "primary", 
                 position: "bottom-right", 
                 type: 'error',
                 duration: 2000,
                 iconPack : 'material',
                 icon : {
                    name : 'close'
                 }
            });
        }
    }
}
